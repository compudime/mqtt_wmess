unit Unit3;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.AppEvnts,
  Vcl.ExtCtrls, Vcl.ComCtrls;

type
  TForm3 = class(TForm)
    Memo1: TMemo;
    btnSend: TButton;
    Memo2: TMemo;
    btnSubscribe: TButton;
    stat1: TStatusBar;
    Panel1: TPanel;
    lbledtPort: TLabeledEdit;
    LabeledEdit1: TLabeledEdit;
    CheckBox1: TCheckBox;
    pnl1: TPanel;
    btn1: TButton;
    btndisconnect: TButton;
    btnUnload: TButton;
    btnLoad: TButton;
    lbledt1: TLabeledEdit;
    mmoMessageRecived: TMemo;
    lbl1: TLabel;
    procedure btnLoadClick(Sender: TObject);
    procedure btnSendClick(Sender: TObject);
    procedure btnSubscribeClick(Sender: TObject);
    procedure btnUnloadClick(Sender: TObject);
  private
    FTisWin3Loaded: Integer;
    FTisWin3Handle: THandle;
  public
    procedure WMCopyData(var Msg: TWMCopyData); message WM_COPYDATA;
  end;

var
  Form3: TForm3;

implementation

{$R *.dfm}

procedure TForm3.btnLoadClick(Sender: TObject);
type
  TLib3Loading = procedure(ListenerClass: PAnsiChar); stdcall;
var
  Loading: TLib3Loading;
begin
  //if FTisWin3Loaded = -1 then
  begin
    FTisWin3Handle := LoadLibrary('MQTTLib.dll');
    if FTisWin3Handle > 0 then
    begin
      FTisWin3Loaded := 1;
      @Loading := GetProcAddress(FTisWin3Handle, 'Loading');
      if @Loading = nil then
      begin
        Exit;
      end;
      Loading(PAnsiChar(AnsiString(Application.ClassName)));
    end
    else
    begin
      FTisWin3Loaded := 0;
      RaiseLastOSError;
    end;
  end;
end;

procedure TForm3.btnSendClick(Sender: TObject);
type
  TLib3Loading = procedure(Topic, Mess: PAnsiChar); stdcall;
var
  PublishMess: TLib3Loading;
begin
  @PublishMess := GetProcAddress(FTisWin3Handle, 'PublishMess');
  if @PublishMess = nil then
  begin
    Exit;
  end;
  PublishMess(PAnsiChar(AnsiString('compudime/TEST/Mqtt_TIS')), PAnsiChar(AnsiString(Memo1.Lines.Text)));
end;

procedure TForm3.btnSubscribeClick(Sender: TObject);
type
  TLib3Loading = procedure(Topic: PAnsiChar); stdcall;
var
  Subscribe: TLib3Loading;
begin
  @Subscribe := GetProcAddress(FTisWin3Handle, 'Subscribe');
  if @Subscribe = nil then
  begin
    Exit;
  end;
  Subscribe(PAnsiChar(AnsiString('compudime/TEST/Mqtt_TIS')));
end;

procedure TForm3.btnUnloadClick(Sender: TObject);
type
  TLib3Loading = procedure; stdcall;
var
  UnLoading: TLib3Loading;
begin
  if FTisWin3Handle > 0 then
  begin
    FTisWin3Loaded := 1;
    @UnLoading := GetProcAddress(FTisWin3Handle, 'UnLoading');
    if @UnLoading = nil then
    begin
      Exit;
    end;
    UnLoading;
  end
end;

procedure TForm3.WMCopyData(var Msg: TWMCopyData);
var
  S: string;
begin
  S := PChar(Msg.CopyDataStruct.lpData);
  Memo2.Lines.Add(S);
  Msg.Result := 1;
end;

end.
