object Form3: TForm3
  Left = 0
  Top = 0
  Caption = 'Form3'
  ClientHeight = 626
  ClientWidth = 908
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 8
    Top = 8
    Width = 185
    Height = 89
    Lines.Strings = (
      'subsribe/topic/all,'
      'other/topic')
    TabOrder = 0
  end
  object btnSend: TButton
    Left = 203
    Top = 202
    Width = 75
    Height = 25
    Caption = 'Publish'
    TabOrder = 1
    OnClick = btnSendClick
  end
  object Memo2: TMemo
    Left = 8
    Top = 167
    Width = 185
    Height = 89
    TabOrder = 2
  end
  object btnSubscribe: TButton
    Left = 199
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Subscribe'
    TabOrder = 3
    OnClick = btnSubscribeClick
  end
  object stat1: TStatusBar
    Left = 0
    Top = 607
    Width = 908
    Height = 19
    Panels = <
      item
        Text = 'Not Connected '
        Width = 100
      end>
    ExplicitLeft = 456
    ExplicitTop = 272
    ExplicitWidth = 0
  end
  object Panel1: TPanel
    Left = 8
    Top = 281
    Width = 425
    Height = 137
    Caption = 'Panel1'
    ShowCaption = False
    TabOrder = 5
    object lbl1: TLabel
      Left = 16
      Top = 96
      Width = 372
      Height = 13
      Caption = 
        'include all options found in  http://www.hivemq.com/demos/websoc' +
        'ket-client/'
      WordWrap = True
    end
    object lbledtPort: TLabeledEdit
      Left = 16
      Top = 59
      Width = 105
      Height = 21
      EditLabel.Width = 20
      EditLabel.Height = 13
      EditLabel.Caption = 'Port'
      TabOrder = 0
      Text = '8000'
    end
    object LabeledEdit1: TLabeledEdit
      Left = 16
      Top = 16
      Width = 185
      Height = 21
      EditLabel.Width = 34
      EditLabel.Height = 13
      EditLabel.Caption = 'Broker '
      TabOrder = 1
      Text = 'broker.mqttdashboard.com'
    end
    object CheckBox1: TCheckBox
      Left = 136
      Top = 56
      Width = 97
      Height = 17
      Caption = 'Auto Reconnect'
      TabOrder = 2
    end
  end
  object pnl1: TPanel
    Left = 640
    Top = 8
    Width = 129
    Height = 145
    Caption = 'pnl1'
    ShowCaption = False
    TabOrder = 6
    object btn1: TButton
      AlignWithMargins = True
      Left = 4
      Top = 75
      Width = 121
      Height = 25
      Margins.Top = 12
      Align = alTop
      Caption = 'Connect'
      TabOrder = 0
      ExplicitLeft = 70
      ExplicitTop = 72
      ExplicitWidth = 75
    end
    object btndisconnect: TButton
      AlignWithMargins = True
      Left = 4
      Top = 106
      Width = 121
      Height = 25
      Align = alTop
      Caption = 'Disconnect'
      TabOrder = 1
      ExplicitLeft = 70
      ExplicitTop = 120
      ExplicitWidth = 75
    end
    object btnUnload: TButton
      AlignWithMargins = True
      Left = 4
      Top = 35
      Width = 121
      Height = 25
      Align = alTop
      Caption = 'Unload DLL '
      TabOrder = 2
      OnClick = btnUnloadClick
    end
    object btnLoad: TButton
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 121
      Height = 25
      Align = alTop
      Caption = 'Load DLL'
      TabOrder = 3
      OnClick = btnLoadClick
    end
  end
  object lbledt1: TLabeledEdit
    Left = 203
    Top = 175
    Width = 112
    Height = 21
    EditLabel.Width = 73
    EditLabel.Height = 13
    EditLabel.Caption = 'PublishTo Topic'
    TabOrder = 7
    Text = 'MyPublishTopic/2'
  end
  object mmoMessageRecived: TMemo
    Left = 0
    Top = 448
    Width = 908
    Height = 159
    Align = alBottom
    Lines.Strings = (
      'message received box'
      'Include topic and massge '
      ''
      'Topic: Massage ')
    TabOrder = 8
  end
end
