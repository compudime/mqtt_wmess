unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, TMS.MQTT.Global;

type
  TForm1 = class(TForm)
    procedure FormShow(Sender: TObject);
  private
    procedure MQTTStatus(ASender: TObject; const AConnected: Boolean; AStatus: TTMSMQTTConnectionStatus);
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
  uMQTTLib;

{$R *.dfm}

procedure TForm1.FormShow(Sender: TObject);
begin
  dmMQTT.MQTTClient1.OnConnectedStatusChanged := MQTTStatus;
  dmMQTT.MQTTClient1.Connect;
end;

procedure TForm1.MQTTStatus(ASender: TObject; const AConnected: Boolean; AStatus: TTMSMQTTConnectionStatus);
begin
  if AConnected then
  begin
    dmMQTT.MQTTClient1.OnConnectedStatusChanged := nil;
    Close;
  end;
end;

end.
