library MQTTLib;

uses
  System.SysUtils,
  System.Classes,
  uMQTTLib in 'uMQTTLib.pas' {dmMQTT: TDataModule},
  Unit1 in 'Unit1.pas' {Form1};

{$R *.res}

procedure Loading(ListenerClass: PAnsiChar); stdcall;
begin
  if not Assigned(dmMQTT) then
    dmMQTT := TdmMQTT.Create(nil);

  if ListenerClass <> '' then
    dmMQTT.ListenerClass := string(ListenerClass);

  if not Assigned(Form1) then
    Form1 := TForm1.Create(nil);
  Form1.ShowModal;
end;

procedure Unloading; stdcall;
begin
  if Assigned(dmMQTT) then
    dmMQTT.Free;
  if Assigned(Form1) then
    Form1.Free;
end;

procedure SubscribeToTopic(ATopic: PAnsiChar); stdcall;
begin
  if Assigned(dmMQTT) then
    dmMQTT.Subscribe(string(ATopic));
end;

procedure PublishMess(ATopic, AMess: PAnsiChar); stdcall;
begin
  if Assigned(dmMQTT) then
    dmMQTT.PublishMess(string(ATopic), string(AMess));
end;

exports
  Loading, Unloading, SubscribeToTopic, PublishMess;

begin
end.
