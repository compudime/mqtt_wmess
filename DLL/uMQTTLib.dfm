object dmMQTT: TdmMQTT
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 150
  Width = 215
  object MQTTClient1: TTMSMQTTClient
    BrokerHostName = 'broker.hivemq.com'
    KeepAliveSettings.KeepAliveInterval = 30
    KeepAliveSettings.AutoReconnect = True
    KeepAliveSettings.AutoReconnectInterval = 10
    OnPublishReceived = MQTTClient1PublishReceived
    Version = '1.1.0.2'
    Left = 88
    Top = 56
  end
end
