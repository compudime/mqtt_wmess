unit uMQTTLib;

interface

uses
  System.SysUtils, System.Classes, TMS.MQTT.Global, TMS.MQTT.Client, Winapi.Windows;

type
  TdmMQTT = class(TDataModule)
    MQTTClient1: TTMSMQTTClient;
    procedure DataModuleCreate(Sender: TObject);
    procedure MQTTClient1PublishReceived(ASender: TObject; APacketID: Word;
        ATopic: string; APayload: TArray<System.Byte>);
  private
    FListenerClass: string;
  public
    procedure PublishMess(const ATopic, AMess: string);
    procedure Subscribe(const ATopic: string);
    property ListenerClass: string read FListenerClass write FListenerClass;
  end;

var
  dmMQTT: TdmMQTT;

implementation

uses
  Winapi.Messages;

{%CLASSGROUP 'System.Classes.TPersistent'}

{$R *.dfm}

procedure SendData(const AWindowClass, AText: string);
var
  AHandle: HWND;
  CopyStruc: TCopyDataStruct;
begin
  AHandle := FindWindow(PChar(AWindowClass), nil) ;

  if AHandle > 0 then
  begin
    CopyStruc.dwData := 15;
    CopyStruc.lpData := PAnsiChar(AnsiString(AText));
    CopyStruc.cbData := Length(AText);
    SendMessage(AHandle, WM_COPYDATA, 0, Integer(@CopyStruc));
  end;
end;

procedure TdmMQTT.DataModuleCreate(Sender: TObject);
begin
  //MQTTClient1.Connect;
end;

procedure TdmMQTT.PublishMess(const ATopic, AMess: string);
begin
  MQTTClient1.Publish(ATopic, AMess);
end;

procedure TdmMQTT.Subscribe(const ATopic: string);
begin
  MQTTClient1.Subscribe(ATopic);
end;

procedure TdmMQTT.MQTTClient1PublishReceived(ASender: TObject;
    APacketID: Word; ATopic: string; APayload: TArray<System.Byte>);
begin
  if ListenerClass<>'' then
    SendData(ListenerClass, TEncoding.UTF8.GetString(APayload));
end;

end.
